package c.mars.fitlib;

import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import c.mars.fitlib.common.Display;

/**
 * Created by Constantine Mars on 1/19/2015.
 */
public class History {
    private GoogleApiClient client;
    private Display display;
    private String email;
    private AtomicLong idCounter = new AtomicLong();

    private static final String TAG = History.class.getName();

    public History(GoogleApiClient client, Display display, String email) {
        this.client = client;
        this.display = display;
        this.email = email;
    }

    public void readWeekBefore(Date date) {
        Calendar cal = Calendar.getInstance();
//        Date now = new Date();
        cal.setTime(date);
        long endTime = cal.getTimeInMillis();
        cal.add(Calendar.WEEK_OF_YEAR, -1);
        long startTime = cal.getTimeInMillis();

        read(startTime, endTime);
    }

    public void read(long start, long end) {

        final DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");// SimpleDateFormat.getDateInstance();
        display.show("Showing Details for " + email);
        display.show("history reading range: " + dateFormat.format(start) + " - " + dateFormat.format(end));

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(start, end, TimeUnit.MILLISECONDS)
                .build();

        DataReadRequest readRequest2 = new DataReadRequest.Builder()
                .aggregate(DataType.TYPE_DISTANCE_DELTA, DataType.AGGREGATE_DISTANCE_DELTA)
                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(start, end, TimeUnit.MILLISECONDS)
                .build();

        ResultCallback<DataReadResult> resultCallback = new ResultCallback<DataReadResult>() {
            @Override
            public void onResult(DataReadResult dataReadResult) {
                if (dataReadResult.getBuckets().size() > 0) {
                    display.show("DataSet.size(): "
                            + dataReadResult.getBuckets().size());
                    for (Bucket bucket : dataReadResult.getBuckets()) {
                        List<DataSet> dataSets = bucket.getDataSets();
                        for (DataSet dataSet : dataSets) {
                            if(!dataSet.getDataPoints().isEmpty())
                                display.show("dataSet.dataType: " + dataSet.getDataType().getName());

                            for (DataPoint dp : dataSet.getDataPoints()) {
                                describeDataPoint(dp, dateFormat);
                            }
                        }
                    }
                } else if (dataReadResult.getDataSets().size() > 0) {
                    display.show("dataSet.size(): " + dataReadResult.getDataSets().size());
                    for (DataSet dataSet : dataReadResult.getDataSets()) {
                        if(!dataSet.getDataPoints().isEmpty())
                            display.show("dataType: " + dataSet.getDataType().getName());

                        for (DataPoint dp : dataSet.getDataPoints()) {
                            describeDataPoint(dp, dateFormat);
                        }
                    }
                }

            }
        };
        Fitness.HistoryApi.readData(client, readRequest).setResultCallback(resultCallback);
        Fitness.HistoryApi.readData(client, readRequest2).setResultCallback(resultCallback);
    }

    public void describeDataPoint(DataPoint dp, DateFormat dateFormat) {
        StringBuilder msg = new StringBuilder("dataPoint: ");
        msg.append("type: ").append(dp.getDataType().getName()).append("\n");
        msg.append(", range: [").append(dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS))).append("-").append(dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS))).append("]\n");
        msg.append(", fields: [");
        StringBuilder fieldData = new StringBuilder();
        for(Field field : dp.getDataType().getFields()) {
            fieldData.append(field.getName()).append("=").append(dp.getValue(field)).append(" ");
        }
        msg.append(fieldData.toString()).append("]");
        display.show(msg.toString());
        postData(dp.getDataType().getName(), fieldData.toString());
    }

    public void postData(final String dataTypeName, final String fieldData) {
        // Create a new HttpClient and Post Header
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://192.168.1.10:8085/greeting");

        try {
            // Add your data
            JSONObject holder = new JSONObject();
            holder.put("id", idCounter.incrementAndGet());
            holder.put("content", dataTypeName + " - " + fieldData);
            JSONArray array = new JSONArray();
            array.put(holder);
            StringEntity se = new StringEntity(array.toString());

            //sets the post request as the resulting string
            httppost.setEntity(se);
            //sets a request header so the page receving the request
            //will know what to do with it
            httppost.setHeader("Accept", "application/json");
            httppost.setHeader("Content-type", "application/json");

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);
            InputStream responseStream = response.getEntity().getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseStream));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
            Log.d(TAG, out.toString());
        } catch (ClientProtocolException e) {
            Log.e(TAG, "Exception: " + e.getMessage(), e);
        } catch (IOException e) {
            Log.e(TAG, "Exception: " + e.getMessage(), e);
        } catch (JSONException e) {
            Log.e(TAG, "Exception: " + e.getMessage(), e);
        }
    }
}
